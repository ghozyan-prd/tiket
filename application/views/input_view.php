<!DOCTYPE html>
<html lang="en">
<head>
	<title>Input Peserta Lomba</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url().'assets/images/logo1.png'?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/bootstrap/css/bootstrap.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/fonts/iconic/css/material-design-iconic-font.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/animate/animate.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/css-hamburgers/hamburgers.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/animsition/css/animsition.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/select2/select2.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/daterangepicker/daterangepicker.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/vendor/noui/nouislider.min.css'?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/util.css'?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/main.css'?>">
<!--===============================================================================================-->
<link rel="stylesheet" href="<?php echo base_url().'assets/css/jquery-ui.css'?>">
</head>
<body>
	<?php
		$tgl_input = date("d-m-Y");
	?>


	<div class="container-contact100">
		<div class="wrap-contact100">
			    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
      <b> <script type="text/javascript">
    //fungsi displayTime yang dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik
    function tampilkanwaktu(){
        //buat object date berdasarkan waktu saat ini
        var waktu = new Date();
        //ambil nilai jam,
        //tambahan script + "" supaya variable sh bertipe string sehingga bisa dihitung panjangnya : sh.length
        var sh = waktu.getHours() + "";
        //ambil nilai menit
        var sm = waktu.getMinutes() + "";
        //ambil nilai detik
        var ss = waktu.getSeconds() + "";
        //tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
        document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
    }
</script>
<body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">
<span id="clock"></span>
<?php
$hari = date('l');
/*$new = date('l, F d, Y', strtotime($Today));*/
if ($hari=="Sunday") {
	echo "Minggu";
}elseif ($hari=="Monday") {
	echo "Senin";
}elseif ($hari=="Tuesday") {
	echo "Selasa";
}elseif ($hari=="Wednesday") {
	echo "Rabu";
}elseif ($hari=="Thursday") {
	echo("Kamis");
}elseif ($hari=="Friday") {
	echo "Jum'at";
}elseif ($hari=="Saturday") {
	echo "Sabtu";
}
?>,
<?php
$tgl =date('d');
echo $tgl;
$bulan =date('F');
if ($bulan=="January") {
	echo " Januari ";
}elseif ($bulan=="February") {
	echo " Februari ";
}elseif ($bulan=="March") {
	echo " Maret ";
}elseif ($bulan=="April") {
	echo " April ";
}elseif ($bulan=="May") {
	echo " Mei ";
}elseif ($bulan=="June") {
	echo " Juni ";
}elseif ($bulan=="July") {
	echo " Juli ";
}elseif ($bulan=="August") {
	echo " Agustus ";
}elseif ($bulan=="September") {
	echo " September ";
}elseif ($bulan=="October") {
	echo " Oktober ";
}elseif ($bulan=="November") {
	echo " November ";
}elseif ($bulan=="December") {
	echo " Desember ";
}
$tahun=date('Y');
echo $tahun;
?></b>
    </a>
			<form class="contact100-form validate-form" action="<?php echo base_url(). 'index.php/input/tambah'; ?>" method="post"> <br>
				<input type="hidden" name="tgl" class="form-control pull-right"  value="<?= $tgl_input; ?>" required="" readonly="">

<div class="float-right"><a href="<?php echo base_url().'index.php/Book'?>" class="btn btn-primary" ><span class="fa fa-plus"></span> Data Masuk</a>
<a href="<?php echo base_url().'index.php/Login/logout'?>" class="btn btn-danger" ><span class="fa fa-sign-out"></span> Logout</a></div>
				<span class="contact100-form-title"> <img src="<?php echo base_url(); ?>assets/images/logo1.png" width="150px" height="100px" alt="Logo">
					Lomba IFEST 2019
				</span>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Enter Your Peserta">
					<span class="label-input100">KODE BOKING*</span>
					<input  class="input100" type="text" name="kode" class="form-control"  value="<?= $kode; ?>" required="" readonly>
				</div>

				<div class="wrap-input100 validate-input bg1" data-validate="Please Type Panitia Penanggung Jawab">
					<span class="label-input100">PENANGGUNG JAWAB *</span>
					<input class="input100" type="text"  name="pj" value="<?php echo $this->session->userdata('ses_id');?>" placeholder="Panitia Penanggung Jawab">
				</div>

				<div class="wrap-input100 validate-input bg1" data-validate="Please Type Asal Sekolah">
					<span class="label-input100">ASAL SEKOLAH / TEAM GAMERS *</span>
					<input class="input100" type="text" name="sekolah" placeholder="Enter Asal Sekolah / Team Gamers">
				</div>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Enter Nama Peserta">
					<span class="label-input100">PESERTA (1) *</span>
					<input class="input100" type="text" name="peserta1" placeholder="Enter Nama Peserta (1) ">
				</div>

				<div class="wrap-input100  bg1 rs1-wrap-input100">
					<span class="label-input100">PESERTA (2)</span>
					<input class="input100" type="text" name="peserta2" placeholder="Enter Nama Peserta (2)">
				</div>

				<div class="wrap-input100  bg1 rs1-wrap-input100">
					<span class="label-input100">PESERTA (3)</span>
					<input class="input100" type="text" name="peserta3" placeholder="Enter Nama Peserta (3)">
				</div>

				<div class="wrap-input100 validate-input bg1" data-validate="Please Type  Email">
					<span class="label-input100">EMAIL *</span>
					<input class="input100" type="email" name="email" placeholder="Enter Email">
				</div>

				<div class="wrap-input100 validate-input bg1" data-validate="Please Type  No Telphone">
					<span class="label-input100">No Telphone *</span>
					<input class="input100" type="number" name="phone" placeholder="Enter  No Telphone">
				</div>

				<div class="wrap-input100 validate-input bg1" data-validate="Please Type  No Telphone">
	                <span class="label-input100">JENIS TIKET *</span>
									<br>
	                    <select name="jenis" id="kategori">
	                    	<option value="0">-PILIH-</option>
	                    	<?php foreach($data->result() as $row):?>
	                    		<option value="<?php echo $row->kategori_id;?>"><?php echo $row->kategori_id;?></option>
	                    	<?php endforeach;?>
	                    </select>
											<div class="dropDownSelect2"></div>
	            </div>

							<div class="wrap-input100 validate-input bg1">
								<span class="label-input100">BIAYA *</span>
								<br>
								<select name="bayar" class="subkategori" readonly required>
									<option value="0">Rp. 0-</option>
								</select>
							</div>

				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							Submit
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>

				</div>
			</form>
		</div>
	</div>



	<!--===============================================================================================-->
		<script src="<?php echo base_url().'assets/vendor/jquery/jquery-3.2.1.min.js'?>"></script>
	<!--===============================================================================================-->
		<script src="<?php echo base_url().'assets/vendor/animsition/js/animsition.min.js'?>"></script>
	<!--===============================================================================================-->
		<script src="<?php echo base_url().'assets/vendor/bootstrap/js/popper.js'?>"></script>
		<script src="<?php echo base_url().'assets/vendor/bootstrap/js/bootstrap.min.js'?>"></script>
	<!--===============================================================================================-->
		<script src="<?php echo base_url().'assets/vendor/select2/select2.min.js'?>"></script>
		<script>
			$(".js-select2").each(function(){
				$(this).select2({
					minimumResultsForSearch: 20,
					dropdownParent: $(this).next('.dropDownSelect2')
				});


				$(".js-select2").each(function(){
					$(this).on('select2:close', function (e){
						if($(this).val() == "Please chooses") {
							$('.js-show-service').slideUp();
						}
						else {
							$('.js-show-service').slideUp();
							$('.js-show-service').slideDown();
						}
					});
				});
			})
		</script>
	<!--===============================================================================================-->
		<script src="<?php echo base_url().'assets/vendor/daterangepicker/moment.min.js'?>"></script>
		<script src="<?php echo base_url().'assets/vendor/daterangepicker/daterangepicker.js'?>"></script>
	<!--===============================================================================================-->
		<script src="<?php echo base_url().'assets/vendor/countdowntime/countdowntime.js'?>"></script>
	<!--===============================================================================================-->
		<script src="<?php echo base_url().'assets/vendor/noui/nouislider.min.js'?>"></script>
		<script>
		    var filterBar = document.getElementById('filter-bar');

		    noUiSlider.create(filterBar, {
		        start: [ 1500, 3900 ],
		        connect: true,
		        range: {
		            'min': 1500,
		            'max': 7500
		        }
		    });

		    var skipValues = [
		    document.getElementById('value-lower'),
		    document.getElementById('value-upper')
		    ];

		    filterBar.noUiSlider.on('update', function( values, handle ) {
		        skipValues[handle].innerHTML = Math.round(values[handle]);
		        $('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
		        $('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
		    });
		</script>
	<!--===============================================================================================-->
		<script src="<?php echo base_url().'assets/js/main.js'?>"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
	</script>
	<script src="<?php echo base_url().'assets/js/jquery-2.2.3.min.js'?>" type="text/javascript"></script>
	<script src="<?php echo base_url().'assets/js/jquery-3.3.1.js'?>" type="text/javascript"></script>
	<script src="<?php echo base_url().'assets/js/bootstrap.js'?>" type="text/javascript"></script>
	<script src="<?php echo base_url().'assets/js/jquery-ui.js'?>" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#kategori').change(function(){
				var id=$(this).val();
				$.ajax({
					url : "<?php echo base_url();?>index.php/input/get_subkategori",
					method : "POST",
					data : {id: id},
					async : false,
			        dataType : 'json',
					success: function(data){
						var html = '';
			            var i;
			            for(i=0; i<data.length; i++){
			                html += '<option>'+data[i].subkategori_nama+'</option>';
			            }
			            $('.subkategori').html(html);

					}
				});
			});
		});
	</script>


</body>
</html>
