<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends CI_Controller {

	 public function __construct()
	 	{
	 		parent::__construct();
	 		$this->load->library('pdf');
			$this->load->helper('url');
	 		$this->load->model('Model');
			if($this->session->userdata('masuk') != TRUE){
	    $url=base_url();
	    redirect($url);
	  }

	 	}


	public function index()
	{
		if($this->session->userdata('akses')=='2'){
		$data['books']=$this->Model->get_all_books();
		$data['total']=$this->Model->total();
		$data['cc']=$this->Model->cc();
		$data['umum']=$this->Model->umum();
		$data['uin']=$this->Model->uin();
		$data['bp']=$this->Model->bp();
		$data['pubg']=$this->Model->pubg();
		$data['dota']=$this->Model->dota();
		$data['ml']=$this->Model->ml();
		$this->load->view('panitia/book_view',$data);
	}else{
	 $this->load->view('login');
	}

	}
	public function book_add()
		{
			$data = array(
					'kode' => $this->input->post('kode'),
					'sekolah' => $this->input->post('sekolah'),
					'peserta1' => $this->input->post('peserta1'),
					'peserta2' => $this->input->post('peserta2'),
					'peserta3' => $this->input->post('peserta3'),
					'email' => $this->input->post('email'),
					'phone' => $this->input->post('phone'),
					'jenis' => $this->input->post('jenis'),
					'pj' => $this->input->post('pj'),
					'bayar' => $this->input->post('bayar'),
					'tgl' => $this->input->post('tgl'),
				);
			$insert = $this->Model->book_add($data);
			echo json_encode(array("status" => TRUE));
		}
		public function ajax_edit($id)
		{
			$data = $this->Model->get_by_id($id);



			echo json_encode($data);
		}

		public function book_update()
	{
		$data = array(
			'kode' => $this->input->post('kode'),
			'sekolah' => $this->input->post('sekolah'),
			'peserta1' => $this->input->post('peserta1'),
			'peserta2' => $this->input->post('peserta2'),
			'peserta3' => $this->input->post('peserta3'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'jenis' => $this->input->post('jenis'),
			'pj' => $this->input->post('pj'),
			'bayar' => $this->input->post('bayar'),
			'tgl' => $this->input->post('tgl'),
			);
		$this->Model->book_update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function book_delete($id)
	{
		$this->Model->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	function cetak(){
		$id = $this->uri->segment(4);
		$this->db->from('tiket');
		$this->db->where('id',$id);
		$query = $this->db->get();
		$tiket = $query->result();
		foreach ($tiket as $row){

			$pdf = new FPDF('l','mm','A5');
			$pdf->AddPage();
			$pdf->SetLineWidth(0);
			$pdf->Image('assets/images/logo1.png',65,4,32,25);
        $pdf->Image('assets/images/hima.png',105,4,30,25);

			$pdf->Ln(20);
		$pdf->SetFont('Times','B',15);
		$pdf->SetTextColor(0);
		$pdf->Cell(30,10,'IFEST HIMAPROSIF 2019');

		$pdf->Cell(90);
		$pdf->SetFont('times','',10);
		$pdf->SetTextColor(169, 169, 169);
		$pdf->cell(30,10,'To :');

		$pdf->Ln(5);
		$pdf->SetFont('times','',10);
		$pdf->SetTextColor(0);
		$pdf->cell(30,10,'JL A. YANI NO 117 SURABAYA');

		$pdf->Cell(90);
		$pdf->SetFont('times','B',12);
		$pdf->SetTextColor(0);
		$pdf->Cell(30,10,$row->peserta1);

		$pdf->Ln(7);
		$pdf->SetFont('times','i',10);
		$pdf->SetTextColor(0);
		$pdf->Image('assets/images/wa.png',10,45,4,4);
		$pdf->Cell(5);
		$pdf->cell(30,10,'085231041975 (Aulia Nabilah)');

		$pdf->Cell(85);
		$pdf->SetFont('times','',10);
		$pdf->SetTextColor(0);
		$pdf->Cell(30,10,$row->sekolah);

		$pdf->Ln(5);
		$pdf->Image('assets/images/line.png',9.5,50,5,5);
		$pdf->Cell(5);
		$pdf->cell(30,10,'@cisrani');

		$pdf->Cell(85);
		$pdf->SetFont('times','',10);
		$pdf->SetTextColor(0);
		$pdf->Cell(30,10,$row->email);

		$pdf->Ln(5);
		$pdf->SetTextColor(0);
		$pdf->Cell(30,10,'Instagram : @ifest2019');

		$pdf->Cell(90);
		$pdf->SetFont('times','B',12);
		$pdf->SetTextColor(0);
		$pdf->Cell(30,10,'Kode Boking : ' . $row->kode);

		$pdf->Ln(5);
		$pdf->SetFont('times','',10);
		$pdf->SetTextColor(1, 162, 232);
		$pdf->Cell(30,10,'http://ifest.himaprosif.com/     official.ifestuinsa@gmail.com');

		$pdf->Ln(10);
		$pdf->SetLineWidth(0);
    	$pdf->Line(10,65,200,65);
    	$pdf->SetLineWidth(1);
    	$pdf->Line(10,66,200,66);
    	$pdf->SetLineWidth(0);
		$pdf->Line(10,67,200,67);

		$pdf->cell(55);
			$pdf->Ln(5);
			$pdf->SetFont('times','B',12);
			$pdf->SetTextColor(0);
			$pdf->SetFillColor(248, 248, 255);
			$pdf->Cell(15, 7, "No", 'LTR', 0, 'C', true);
			$pdf->Cell(90, 7, "Jenis Tiket", 'LTR', 0, 'C', true);
			$pdf->Cell(43, 7, "Biaya", 'LTR', 0, 'C', true);
			$pdf->Cell(43, 7, "Tax", 'LTR', 0, 'C', true);
			$pdf->Ln(0);

			$pdf->Cell(0);
			$pdf->SetFont('times','B',12);
			$pdf->SetTextColor(0);
			$pdf->SetFillColor(248, 248, 255);
			$pdf->Ln(7);

			$pdf->Cell(15,7,'1',1,0,'C');
        	$pdf->Cell(90,7,$row->jenis,1);
        	$pdf->Cell(43,7,'Rp. '.number_format($row->bayar),1,0,'C');
        	$pdf->Cell(43,7,'-',1,0,'C');
        	$pdf->Ln(8);

        	$pdf->Cell(118);
        	$pdf->Cell(30,10,'Total Biaya :');
        	$pdf->Cell(43,9,'Rp. '.number_format($row->bayar),1,0,'C');

        	$pdf->Ln(10);
			$pdf->SetFont('times','i',10);
			$pdf->SetTextColor(0);
			$pdf->cell(30,10,'* Bukti Pembayaran ini Dicetak Otomatis di Aplikasi IFEST');
			$pdf->Ln(5);
			$pdf->cell(30,10,'* Bukti Pembayaran ini Harap Disimpan');

			$pdf->Cell(120);
			$pdf->SetFont('times','B',12);
			$pdf->SetTextColor(0);
			$pdf->Cell(30,10,'Surabaya,  ' . $row->tgl);

			$pdf->Image('assets/images/stempel.png',165,107,30,25);

			$pdf->Ln(10);
			$pdf->Cell(160);
			$pdf->SetFont('times','',10);
			$pdf->SetTextColor(0);
			$pdf->Cell(30,10,'PANITIA');

			$pdf->Ln(5);
			$pdf->Cell(155);
			$pdf->SetFont('times','B',14);
			$pdf->SetTextColor(0);
			$pdf->Cell(30,10,'IFEST - 2019');



			$pdf->Output('','IFEST/'.$row->kode.'.pdf');
			}
		}



}
