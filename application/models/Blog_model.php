<?php
class Blog_model extends CI_Model{

	function get_all_blog(){
		$result=$this->db->get('lomba');
		return $result;
	}

	function search_blog($title){
		$this->db->like('jenis', $title , 'both');
		$this->db->order_by('jenis', 'ASC');
		$this->db->limit(10);
		return $this->db->get('lomba')->result();
	}

}
